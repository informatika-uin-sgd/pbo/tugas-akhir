# No 1
Mampu mendemonstrasikan penyelesaian masalah dengan pendekatan matematika dan algoritma pemrograman secara tepat.
```java
public void pemindahanSaldo(String nomorTujuan) {
	Rekening rekeningTujuan = BRImoApp.cariRekening(nomorTujuan);
	rekeningTujuan.setSaldoRekening(rekeningTujuan.getSaldoRekening() + nominal);

	Mutasi mutasi = new Mutasi(("BFST " + sumberDana.getNomorRekening()), ("(+) Rp" + nominal), getTanggalWaktuTransaksi());
	rekeningTujuan.tambahMutasi(mutasi);
}
```
Pada class Transfer, terdapat fungsi yang mengubah(update) nilai saldo rekening tujuan.

# No 2
Mampu menjelaskan algoritma dari solusi yang dibuat.

[**BRImo App**](https://gitlab.com/informatika-uin-sgd/pbo/tugas-akhir/-/blob/main/BRImoApp.java)

# No 3
Mampu menjelaskan konsep dasar OOP.

Konsep dasar OOP terdiri dari 4 Pilar, yaitu Encapsulation, Abstraction, Inheritance, dan Polymorphism.

1. Encapsulation = membantu mencegah manipulasi data yang tidak valid dan meningkatkan keamanan dari program.
2. Abstraction = membuat kita untuk fokus pada fitur yang lebih penting dan dengan konsep ini juga dapat mengurangi kompleksitas dari program.
3. Inheritance = mengurangi duplikasi kode dan meningkatkan efisiensi pengembangan aplikasi ketika kita membuat kelas-kelas baru yang lebih spesifik dan terfokus.
4. Polymorphism = meningkatkan fleksibilitas program dan memudahkan dalam pengembangan aplikasi yang lebih kompleks.

Penggunaan OOP dalam projek perusahaan dapat mempermudah dalam pengembangan aplikasi baik itu pembuatan maupun pemeliharaan. Selain itu, OOP mempermudah dalam mengembangkan kode-kode kompleks menjadi lebih mudah digunakan, dibaca, dan secara umum lebih baik.

# No 4
Mampu mendemonstrasikan penggunaan Encapsulation secara tepat.
```java
class Rekening {
	private final JenisTabungan jenisTabungan;
	private final String nomorRekening;
	private String namaRekening;
	private long saldoRekening;
	private Akun akun;
	private ArrayList<Mutasi> mutasi = new ArrayList<>();

	public Rekening(JenisTabungan jenisTabungan, String nomorRekening, String namaRekening, long saldoRekening) {
		this.jenisTabungan = jenisTabungan;
		this.nomorRekening = nomorRekening;
		this.namaRekening = namaRekening;
		this.saldoRekening = saldoRekening;
		this.akun = null;
	}
	public void tambahMutasi(Mutasi mutasi) {
		this.mutasi.add(mutasi);
	}
	public JenisTabungan getJenisTabungan() {
		return jenisTabungan;
	}
	public String getNomorRekening() {
		return nomorRekening;
	}
	public void setNamaRekening(String namaRekening) {
		this.namaRekening = namaRekening;
	}
	public String getNamaRekening() {
		return namaRekening;
	}
	public void setSaldoRekening(long saldoRekening) {
		this.saldoRekening = saldoRekening;
	}
	public long getSaldoRekening() {
		return this.saldoRekening;
	}
	public void setAkun(Akun akun) {
		this.akun = akun;
	}
	public Akun getAkun() {
		return akun;
	}
	public ArrayList<Mutasi> getMutasiRekening() {
		return mutasi;
	}
}
```
Penggunaan encapsulation di sini sangat berguna, terutama untuk menjaga data saldoRekening agar tidak dapat secara langsung dimanipulasi dari luar.
# No 5
Mampu mendemonstrasikan penggunaan Abstraction secara tepat.
```java
abstract class Transaksi {
	private static int nomorReferensiCounter = 1;

	private String tanggalWaktuTransaksi;
	private String nomorReferensi;
	protected Rekening sumberDana;
	protected String nomorTujuan;
	protected long nominal;
	protected int biayaAdmin;
	protected long totalBiaya;
	
	public Transaksi() {
		this.tanggalWaktuTransaksi = getTanggalWaktu();
		this.nomorReferensi = String.format("%012d", nomorReferensiCounter++);
	}
	private String getTanggalWaktu() {
        TimeZone zonaWaktu = TimeZone.getTimeZone("GMT+7");
        Calendar kalender = Calendar.getInstance(zonaWaktu);
        SimpleDateFormat tanggalWaktu = new SimpleDateFormat("dd MMM yyyy | HH:mm:ss");

        return tanggalWaktu.format(kalender.getTime()) + " WIB";
	}
	public void penguranganSaldo(Rekening rekening) {
		rekening.setSaldoRekening(rekening.getSaldoRekening() - totalBiaya);
	}
	public String getTanggalWaktuTransaksi() {
		return tanggalWaktuTransaksi;
	}
	public String getNomorReferensi() {
		return nomorReferensi;
	}
	public abstract String getJenisTransaksi();
}
```
Pembuatan abstraction dari class Transaksi di sini dimaksudkan untuk menggambarkan secara umum bagaimana seluruh transaksi yang ada di aplikasi bekerja. 
# No 6
Mampu mendemonstrasikan penggunaan Inheritance dan Polymorphism secara tepat.
```java
class Aktivitas extends Mutasi {
	private String namaTransaksi;
    private String nomorReferensi;
    private String tanggalWaktu;

	public Aktivitas(String namaTransaksi, String keterangan, String nomorReferensi, String tanggalWaktu) {
		super(keterangan);
		this.namaTransaksi = namaTransaksi;
		this.nomorReferensi = nomorReferensi;
		this.tanggalWaktu = formatTanggalWaktu(tanggalWaktu);
	}
	@Override
	protected String formatTanggalWaktu(String tanggalWaktu) {
        tanggalWaktu = tanggalWaktu.substring(0, 21);
        SimpleDateFormat formatTanggalWaktu = new SimpleDateFormat("dd MMM yyyy | HH:mm:ss", new Locale("id"));
        Date hasil;
        String tanggalBaru;

        try {
            hasil = formatTanggalWaktu.parse(tanggalWaktu);
            formatTanggalWaktu.applyLocalizedPattern("dd MMM yyyy HH:mm:ss");
            tanggalBaru = formatTanggalWaktu.format(hasil);
        } catch (Exception e) {
            tanggalBaru = "Error: " + e.getMessage();
        }
        return tanggalBaru;
    }
	public String getNamaTransaksi() {
		return namaTransaksi;
	}
	public String getNomorReferensi() {
		return nomorReferensi;
	}
	@Override
	public String getTanggalWaktu() {
		return tanggalWaktu;
	}
}
```
Penggunaan inheritance di sini dimaksudkan untuk mempersedikit kode yang dibuat, elemen-elemen pada Mutasi dan Aktivitas dirasa hampir mirip dan kedudukan Mutasi lebih tinggi dibanding Aktivitas. Sehingga, saya buat Aktivitas menjadi turunan dari Mutasi dengan sedikit tambahan atribut dan method. Untuk method overridenya (formatTanggalWaktu()) sendiri dibuat untuk membedakan format tanggal yang dihasilkan.

Method overloading pada kelas Akun
```java
class Akun {
	public void tambahRekening() {
		rekening.add(BRImoApp.bukaRekening());
		rekening.get(rekening.size() - 1).setAkun(this);
	}
	public void tambahRekening(Rekening rekening) {
		this.rekening.add(rekening);
		this.rekening.get(0).setAkun(this);
	}
}
```
Penggunaan method overloading di sini dimaksudkan untuk membedakan skema yang sedang dijalankan. Method pertama digunakan untuk pengguna lama yang ingin membuka rekening baru, sedangkan method kedua digunakan untuk pengguna yang baru bergabung ke aplikasi.

# No 7
Mampu menerjemahkan proses bisnis ke dalam skema OOP secara tepat. Bagaimana cara Kamu mendeskripsikan proses bisnis (kumpulan use case) ke dalam OOP?
1. Pertama-tama menganalisis aplikasi lalu membuat list use case dari aplikasi yang akan di-reverse engineering. 
2. Misalnya saya memilih aplikasi BRImo, sehingga contoh use case-nya "user dapat melakukan transaksi berupa transfer, topup, dan bayar va". 
3. Setelah itu kita analisis setiap use case dan elemen-elemen apa saja yang dapat/harus dijadikan sebuah objek. 
4. Untuk use case tadi, kita dapat membuat 'transaksi' sebagai sebuah kelas abstrak, karena transaksi merupakan gambaran umum dari proses bisnis yang terjadi di aplikasi BRImo. 
5. Abstrak kelas ini berisi atribut-atribut yang umum seperti tanggal dan waktu transaksi, dan nantinya akan di-inherit ke kelas 'transfer', 'topuppulsa', dan 'tagihanbriva'. 
6. Pada kelas turunan akan lebih dirincikan atribut yang digunakannya, seperti pada 'transfer' ada atribut bank  tujuan, pada 'topuppulsa' ada atribut provider, dan pada 'tagihanbriva' ada atribut institusi. 
7. Lalu untuk modifier-nya, pada kelas turunan kita beri hak akses private untuk setiap atributnya, sedangkan pada kelas abstraknya ada beberapa atribut seperti nominal, biaya admin, dan total biaya yang diberi hak akses protected, agar atribut tersebut dapat diturunkan ke kelas turunan selagi menjaga keamanan data/nilai di dalamnya.

# No 8
Mampu menjelaskan rancangan dalam bentuk Class Diagram, dan Use Case table

[**Use Case**](https://gitlab.com/informatika-uin-sgd/pbo/tugas-akhir/-/tree/main/#use-case)

[**Class Diagram**](https://gitlab.com/informatika-uin-sgd/pbo/tugas-akhir/-/tree/main/#class-diagram)

# No 9
Mampu memberikan gambaran umum aplikasi kepada publik menggunakan presentasi berbasis video

[![Demo Projek: Reverse Engineering Aplikasi Mobile "BRImo"](https://img.youtube.com/vi/dWp_eyHymwI/0.jpg)](https://youtu.be/dWp_eyHymwI)

# No 10
Inovasi UX

### Membuat rekening dilanjut membuat akun.
![Membuat rekening dan akun](assets/gif/create_account.gif)

### Melakukan transfer uang.
![Melakukan transfer uang](assets/gif/transfer.gif)

### Melihat mutasi rekening dan aktivitas akun.
![Melihat mutasi rekening dan aktivitas akun](assets/gif/mutation_activity.gif)

### Melihat saldo seluruh rekening terdaftar di aplikasi. (Tambahan)
![Melihat saldo seluruh rekening terdaftar di aplikasi](assets/gif/registered_bank_account.gif)