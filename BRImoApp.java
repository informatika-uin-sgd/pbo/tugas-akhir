import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Random;
import java.util.Scanner;
import java.util.TimeZone;

class JenisTabungan {
	private final String namaJenisTabungan;
	private final int saldoMinimum;
	private final int biayaPemeliharaan;

	public JenisTabungan(String namaJenisTabungan, int saldoMinimum, int biayaPemeliharaan) {
		this.namaJenisTabungan = namaJenisTabungan;
		this.saldoMinimum = saldoMinimum;
		this.biayaPemeliharaan = biayaPemeliharaan;
	}
	public String getNamaJenisTabungan() {
		return namaJenisTabungan;
	}
	public int getSaldoMinimum() {
	    return saldoMinimum;
	}
	public int getBiayaPemeliharaan() {
	    return biayaPemeliharaan;
	}
}
class Rekening {
	private final JenisTabungan jenisTabungan;
	private final String nomorRekening;
	private String namaRekening;
	private long saldoRekening;
	private Akun akun;
	private ArrayList<Mutasi> mutasi = new ArrayList<>();

	public Rekening(JenisTabungan jenisTabungan, String nomorRekening, String namaRekening, long saldoRekening) {
		this.jenisTabungan = jenisTabungan;
		this.nomorRekening = nomorRekening;
		this.namaRekening = namaRekening;
		this.saldoRekening = saldoRekening;
		this.akun = null;
	}
	public void tambahMutasi(Mutasi mutasi) {
		this.mutasi.add(mutasi);
	}
	public JenisTabungan getJenisTabungan() {
		return jenisTabungan;
	}
	public String getNomorRekening() {
		return nomorRekening;
	}
	public void setNamaRekening(String namaRekening) {
		this.namaRekening = namaRekening;
	}
	public String getNamaRekening() {
		return namaRekening;
	}
	public void setSaldoRekening(long saldoRekening) {
		this.saldoRekening = saldoRekening;
	}
	public long getSaldoRekening() {
		return this.saldoRekening;
	}
	public void setAkun(Akun akun) {
		this.akun = akun;
	}
	public Akun getAkun() {
		return akun;
	}
	public ArrayList<Mutasi> getMutasiRekening() {
		return mutasi;
	}
}
class Akun {
	private String namaPengguna;
	private String nomorTelepon;
	private String alamatEmail;
	private String username;
	private String kataSandi;
	private String nomorPIN;
	private ArrayList<Rekening> rekening = new ArrayList<>();
	private ArrayList<Aktivitas> aktivitas = new ArrayList<>();

	public Akun(String namaPengguna, String nomorTelepon, String alamatEmail, String username, String kataSandi, String nomorPIN) {
		this.namaPengguna = namaPengguna;
		this.nomorTelepon = nomorTelepon;
		this.alamatEmail = alamatEmail;
		this.username = username;
		this.kataSandi = kataSandi;
		this.nomorPIN = nomorPIN;
	}
	public boolean validasiKataSandi(String kataSandi) {
		return getKataSandi().equals(kataSandi);
	}
	public void tambahRekening() {
		rekening.add(BRImoApp.bukaRekening());
		rekening.get(rekening.size() - 1).setAkun(this);
	}
	public void tambahRekening(Rekening rekening) {
		this.rekening.add(rekening);
		this.rekening.get(0).setAkun(this);
	}
	public void tampilkanListRekening() {
		for (int i = 0; i < rekening.size(); i++) {
			Rekening currentRekening = rekening.get(i);

			System.out.println("-----------------------------------");
			System.out.println((i+1) + ". " + currentRekening.getNomorRekening() + " " + currentRekening.getJenisTabungan().getNamaJenisTabungan());
			System.out.println("   " + currentRekening.getNamaRekening());
			System.out.println("   " + currentRekening.getSaldoRekening());
			System.out.println("-----------------------------------");
		}
	}
	public boolean cekKecukupanSaldo(Rekening rekening, long biaya) {
		if (rekening.getSaldoRekening() < biaya) {
			System.out.println("-----------------------------------");
			System.out.println("Maaf, saldo Anda tidak mencukupi");
			System.out.println("untuk melakukan transaksi ini.");
			System.out.println("Transaksi dibatalkan.");
			System.out.println("-----------------------------------");

			return false;
		} else if (rekening.getSaldoRekening() - biaya < rekening.getJenisTabungan().getSaldoMinimum()) {
			System.out.println("-----------------------------------");
			System.out.println("Maaf, saldo Anda tidak dapat dipa-");
			System.out.println("kai karena merupakan saldo minimum.");
			System.out.println("Transaksi dibatalkan.");
			System.out.println("-----------------------------------");

			return false;
		}
		return true;
	}
	public boolean validasiPIN(String nomorPIN) {
		return getNomorPin().equals(nomorPIN);
	}
	public void tambahAktivitas(Aktivitas aktivitas) {
		this.aktivitas.add(aktivitas);
	}
	public void setNamaPengguna(String namaPengguna) {
		this.namaPengguna = namaPengguna;
	}
	public String getNamaPengguna() {
		return namaPengguna;
	}
	public void setNomorTelepon(String nomorTelepon) {
		this.nomorTelepon = nomorTelepon;
	}
	public String getNomorTelepon() {
		return nomorTelepon;
	}
	public void setEmail(String alamatEmail) {
		this.alamatEmail = alamatEmail;
	}
	public String getEmail() {
		return alamatEmail;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getUsername() {
		return username;
	}
	public void setKataSandi(String kataSandi) {
		this.kataSandi = kataSandi;
	}
	public String getKataSandi() {
		return kataSandi;
	}
	public void setNomorPin(String nomorPIN) {
		this.nomorPIN = nomorPIN;
	}
	public String getNomorPin() {
		return nomorPIN;
	}
	public ArrayList<Rekening> getRekening() {
		return rekening;
	}
	public ArrayList<Aktivitas> getAktivitas() {
		return aktivitas;
	}
}
interface BuktiTransaksi {
	public void tampilkanBuktiTransaksi();
}
abstract class Transaksi {
	private static int nomorReferensiCounter = 1;

	private String tanggalWaktuTransaksi;
	private String nomorReferensi;
	protected Rekening sumberDana;
	protected String nomorTujuan;
	protected long nominal;
	protected int biayaAdmin;
	protected long totalBiaya;
	
	public Transaksi() {
		this.tanggalWaktuTransaksi = getTanggalWaktu();
		this.nomorReferensi = String.format("%012d", nomorReferensiCounter++);
	}
	private String getTanggalWaktu() {
        TimeZone zonaWaktu = TimeZone.getTimeZone("GMT+7");
        Calendar kalender = Calendar.getInstance(zonaWaktu);
        SimpleDateFormat tanggalWaktu = new SimpleDateFormat("dd MMM yyyy | HH:mm:ss");

        return tanggalWaktu.format(kalender.getTime()) + " WIB";
	}
	public void penguranganSaldo(Rekening rekening) {
		rekening.setSaldoRekening(rekening.getSaldoRekening() - totalBiaya);
	}
	public String getTanggalWaktuTransaksi() {
		return tanggalWaktuTransaksi;
	}
	public String getNomorReferensi() {
		return nomorReferensi;
	}
	public abstract String getJenisTransaksi();
}
class Transfer extends Transaksi implements BuktiTransaksi {
	private String bankTujuan;
	private String namaTujuan;

	public Transfer(String bankTujuan, String nomorTujuan, String namaTujuan, long nominal, Rekening sumberDana, int biayaAdmin, long totalBiaya) {
		super();
		this.bankTujuan = bankTujuan;
		this.nomorTujuan = nomorTujuan;
		this.namaTujuan = namaTujuan;
		this.nominal = nominal;
		this.sumberDana = sumberDana;
		this.biayaAdmin = biayaAdmin;
		this.totalBiaya = totalBiaya;
	}
	public void pemindahanSaldo(String nomorTujuan) {
		Rekening rekeningTujuan = BRImoApp.cariRekening(nomorTujuan);
		rekeningTujuan.setSaldoRekening(rekeningTujuan.getSaldoRekening() + nominal);

		Mutasi mutasi = new Mutasi(("BFST " + sumberDana.getNomorRekening()), ("(+) Rp" + nominal), getTanggalWaktuTransaksi());
		rekeningTujuan.tambahMutasi(mutasi);
	}
	public String getJenisTransaksi() {
		return "Transfer BI-FAST";
	}
	public void tampilkanBuktiTransaksi() {
		System.out.println("===================================");
		System.out.println("               BRImo");
		System.out.println("        Transaksi Berhasil\n");
		System.out.println("Tanggal  " + getTanggalWaktuTransaksi());
		System.out.println("Nomor Referensi:       " + getNomorReferensi());
		System.out.println("-----------------------------------");
		System.out.println("Sumber Dana    : " + sumberDana.getNamaRekening());
		System.out.println("                 " + sumberDana.getNomorRekening());
		System.out.println("Jenis Transaksi: " + getJenisTransaksi());
		System.out.println("Bank Tujuan    : " + bankTujuan);
		System.out.println("Nomor Tujuan   : " + nomorTujuan);
		System.out.println("Nama Tujuan    : " + namaTujuan);
		System.out.println("-----------------------------------");
		System.out.println("Nominal        : " + nominal);
		System.out.println("Biaya Admin    : " + biayaAdmin);
		System.out.println("-----------------------------------");
		System.out.println("Total          : " + totalBiaya);
		System.out.println("===================================");
	}
}
class TagihanBRIVA extends Transaksi implements BuktiTransaksi {
	private String institusi;
	private String nomorBRIVA;

	public TagihanBRIVA(String nomorBRIVA, String institusi, long nominal, Rekening sumberDana, int biayaAdmin, long totalBiaya) {
		super();
		this.nomorBRIVA = nomorBRIVA;
		this.institusi = institusi;
		this.nominal = nominal;
		this.sumberDana = sumberDana;
		this.biayaAdmin = biayaAdmin;
		this.totalBiaya = totalBiaya;
	}
	public static String cekInstitusi(String nomorBRIVA) {
		char id1 = nomorBRIVA.charAt(1), id2 = nomorBRIVA.charAt(2);
		if (id1 == '2') {
			if (id2 == '0') {
				return "BUMN";
			} else if (id2 == '1') {
				return "PLN";
			} else if (id2 == '2') {
				return "UIN SGD Bandung";
			}  else if (id2 == '7') {
				return "Tokopedia";
			} else if (id2 == '8') {
				return "SHOPEE";
			} else {
			    return "Universal";
			}
		} else {
			return "Universal";
		}
	}
	public static int cekTagihan(String institusi) {
		if (institusi.equals("BUMN")) {
			return 583500;
		} else if (institusi.equals("PLN")) {
			return 345000;
		} else if (institusi.equals("UIN SGD Bandung")) {
			return 3263000;
		} else if (institusi.equals("Tokopedia")) {
			return 985000;
		} else if (institusi.equals("SHOPEE")) {
			return 101500;
		} else {
			return 67000;
		}
	}
	public String getJenisTransaksi() {
		return "Pembayaran BRIVA";
	}
	public void tampilkanBuktiTransaksi() {
		System.out.println("===================================");
		System.out.println("               BRImo");
		System.out.println("        Transaksi Berhasil\n");
		System.out.println("Tanggal  " + getTanggalWaktuTransaksi());
		System.out.println("Nomor Referensi:       " + getNomorReferensi());
		System.out.println("-----------------------------------");
		System.out.println("Sumber Dana    : " + sumberDana.getNamaRekening());
		System.out.println("                 " + sumberDana.getNomorRekening());
		System.out.println("Jenis Transaksi: " + getJenisTransaksi());
		System.out.println("Institusi      : " + institusi);
		System.out.println("Nomor BRIVA    : " + nomorBRIVA);
		System.out.println("-----------------------------------");
		System.out.println("Nominal        : " + nominal);
		System.out.println("Biaya Admin    : " + biayaAdmin);
		System.out.println("-----------------------------------");
		System.out.println("Total          : " + totalBiaya);
		System.out.println("\n Biaya Termasuk PPN");
		System.out.println(" PT. Bank Rakyat Indonesia Tbk.");
		System.out.println(" Kantor Pusat BRI - Jakarta Pusat");
		System.out.println(" NPWP : 01.001.608.7-051.000");
		System.out.println("===================================");
	}
}
class TopUpPulsa extends Transaksi implements BuktiTransaksi {
	private String provider;

	public TopUpPulsa(String nomorTujuan, String provider, long nominal, Rekening sumberDana, int biayaAdmin, long totalBiaya) {
		super();
		this.nomorTujuan = nomorTujuan;
		this.provider = provider;
		this.nominal = nominal;
		this.sumberDana = sumberDana;
		this.biayaAdmin = biayaAdmin;
		this.totalBiaya = totalBiaya;
	}
	public static String cekProvider(String nomorTujuan) {
		char id1 = nomorTujuan.charAt(2), id2 = nomorTujuan.charAt(3);
		if (id1 == '1') {
			if (id2 == '7' || id2 == '8' || id2 == '9') {
				return "XL";
			} else {
				return "Telkomsel";
			}
		} else if (id1 == '2') {
			return "Telkomsel";
		} else if (id1 == '3') {
			return "AXIS";
		} else if (id1 == '5') {
			if (id2 == '6' || id2 == '7') {
				return "Indosat Ooredoo";
			} else if (id2 == '9') {
				return "XL";
			} else {
				return "Telkomsel";
			}
		} else if (id1 == '7') {
			return "XL";
		} else if (id1 == '8') {
			return "Smartfren";
		} else if (id1 == '9') {
			return "Three";
		} else {
			return "Universal";
		}
	}
	public static int cekBiayaAdmin(String provider) {
		if (provider.equals("Telkomsel")) {
			return 1500;
		} else if (provider.equals("XL")) {
			return 1000;
		} else if (provider.equals("AXIS")) {
			return 500;
		} else if (provider.equals("Indosat Ooredoo")) {
			return 0;
		} else if (provider.equals("Smartfren")) {
			return 2000;
		} else if (provider.equals("Three")) {
			return 0;
		} else {
			return 5000;
		}
	}
	public String getJenisTransaksi() {
		return "Top Up Pulsa";
	}
	public void tampilkanBuktiTransaksi() {
		System.out.println("===================================");
		System.out.println("               BRImo");
		System.out.println("        Transaksi Berhasil\n");
		System.out.println("Tanggal  " + getTanggalWaktuTransaksi());
		System.out.println("Nomor Referensi:       " + getNomorReferensi());
		System.out.println("-----------------------------------");
		System.out.println("Sumber Dana    : " + sumberDana.getNamaRekening());
		System.out.println("                 " + sumberDana.getNomorRekening());
		System.out.println("Jenis Transaksi: " + getJenisTransaksi());
		System.out.println("Nomor Tujuan   : " + nomorTujuan);
		System.out.println("Provider       : " + provider);
		System.out.println("-----------------------------------");
		System.out.println("Nominal        : " + nominal);
		System.out.println("Biaya Admin    : " + biayaAdmin);
		System.out.println("-----------------------------------");
		System.out.println("Total          : " + totalBiaya);
		System.out.println("\n Pulsa Anda akan bertambah secara");
		System.out.println("             otomatis.");
		System.out.println("     Struk ini merupakan bukti");
		System.out.println("        pembayaran yang sah.");
		System.out.println("===================================");
	}
}
class Mutasi {
	private String keterangan;
    private String nominal;
    private String tanggalWaktu;

	public Mutasi(String keterangan) {
		this.keterangan = keterangan;
	}
    public Mutasi(String keterangan, String nominal, String tanggalWaktu) {
        this.keterangan = keterangan;
        this.nominal = nominal;
        this.tanggalWaktu = formatTanggalWaktu(tanggalWaktu);
    }
	protected String formatTanggalWaktu(String tanggalWaktu) {
        tanggalWaktu = tanggalWaktu.substring(0, 21);
        SimpleDateFormat formatTanggalWaktu = new SimpleDateFormat("dd MMM yyyy | HH:mm:ss");
        Date hasil;
        String tanggalBaru;

        try {
            hasil = formatTanggalWaktu.parse(tanggalWaktu);
            formatTanggalWaktu.applyLocalizedPattern("dd/MM/yyyy | HH:mm:ss");
            tanggalBaru = formatTanggalWaktu.format(hasil);
        } catch (Exception e) {
            tanggalBaru = "Error: " + e.getMessage();
        }
        return tanggalBaru + " WIB";
    }
	public String getKeterangan() {
		return keterangan;
	}
	public String getNominal() {
		return nominal;
	}
	public String getTanggalWaktu() {
		return tanggalWaktu;
	}
}
class Aktivitas extends Mutasi {
	private String namaTransaksi;
    private String nomorReferensi;
    private String tanggalWaktu;

	public Aktivitas(String namaTransaksi, String keterangan, String nomorReferensi, String tanggalWaktu) {
		super(keterangan);
		this.namaTransaksi = namaTransaksi;
		this.nomorReferensi = nomorReferensi;
		this.tanggalWaktu = formatTanggalWaktu(tanggalWaktu);
	}
	@Override
	protected String formatTanggalWaktu(String tanggalWaktu) {
        tanggalWaktu = tanggalWaktu.substring(0, 21);
        SimpleDateFormat formatTanggalWaktu = new SimpleDateFormat("dd MMM yyyy | HH:mm:ss");
        Date hasil;
        String tanggalBaru;

        try {
            hasil = formatTanggalWaktu.parse(tanggalWaktu);
            formatTanggalWaktu.applyLocalizedPattern("dd MMM yyyy HH:mm:ss");
            tanggalBaru = formatTanggalWaktu.format(hasil);
        } catch (Exception e) {
            tanggalBaru = "Error: " + e.getMessage();
        }
        return tanggalBaru;
    }
	public String getNamaTransaksi() {
		return namaTransaksi;
	}
	public String getNomorReferensi() {
		return nomorReferensi;
	}
	@Override
	public String getTanggalWaktu() {
		return tanggalWaktu;
	}
}
public class BRImoApp {
	private static Scanner in = new Scanner(System.in);
	private static Scanner str = new Scanner(System.in);
	private static ArrayList<Akun> akunTerdaftar = new ArrayList<>();
	private static ArrayList<Rekening> rekeningTerdaftar = new ArrayList<>();

	public static void main(String[] args) {
		loadData();
		start();
	}
	private static void start() {
		boolean isStarted = true;

		while (isStarted) {
			System.out.println("===================================");
			System.out.println("Selamat Datang di BRImo!");
			System.out.println("===================================");
			System.out.println("1. Punya Akun");
			System.out.println("2. Belum Punya Akun");
			System.out.println("0. Keluar");
			System.out.println("-----------------------------------");
			System.out.print("Pilihan: ");
			int pilihan = in.nextInt();
			
			if (pilihan == 1) {
				login();
			} else if (pilihan == 2) {
				System.out.println("-----------------------------------");
				System.out.println("Apakah Kamu memiliki Rekening BRI");
				System.out.println("yang sedang Aktif?");
				System.out.println("-----------------------------------");
				System.out.println("1. Ya, Saya Punya Rekening BRI");
				System.out.println("2. Belum Punya");
				System.out.println("-----------------------------------");
				System.out.print("Pilihan: ");
				pilihan = in.nextInt();

				if (pilihan == 1) {
					buatAkun();
				} else if (pilihan == 2) {
					bukaRekening();
					buatAkun();
				}
			} else if (pilihan == 0) {
				isStarted = false;
				System.exit(0);
			} else if (pilihan == 12345) {
				sleep(500);
				menuAdmin();
			}
		}	
	}
	private static void login() {
		boolean isLoggedIn = false;

		while (!isLoggedIn) {
			System.out.println("===================================");
			System.out.println("=              Login              =");
			System.out.println("===================================");
			System.out.print("Username: ");
			String username = str.nextLine();

			System.out.print("Password: ");
			String kataSandi = str.nextLine();

			System.out.println();
			Akun akun = cariAkun(username); // cek apakah username ada

			if (akun == null || !akun.validasiKataSandi(kataSandi)) { // cek kataSandi
				System.out.println("-----------------------------------");
				System.out.println("Username atau Password salah");
				System.out.println("-----------------------------------");
				System.out.println("Lupa Username/Password? (1.Ya/2.Tdk)");
				System.out.println("-----------------------------------");
				System.out.print("Pilihan: ");
				int pilihan = in.nextInt();

				if (pilihan == 1) {
					System.out.println("===================================");
					System.out.println("Pilih Lupa Username atau Password");
					System.out.println("sesuai dengan kebutuhan kamu");
					System.out.println("-----------------------------------");
					System.out.println("1. Lupa Username");
					System.out.println("2. Lupa Password");
					System.out.println("-----------------------------------");
					System.out.print("Pilihan: ");
					pilihan = in.nextInt();

					if (pilihan == 1) {
						System.out.println("-----------------------------------");
						System.out.println("Data Akun||");
						System.out.println("-----------------------------------");
						System.out.println("Pastikan data akun yang kamu isikan");
						System.out.println("di bawah telah terdaftar di BRImo");
						System.out.print("\nNomor Rekening: ");
						String nomorRekening = str.nextLine();
						Rekening rekening = cariRekening(nomorRekening);

						if (rekening != null) {
							if (rekening.getAkun() != null) {
								System.out.println("-----------------------------------");
								System.out.println("Data akun ditemukan");
								System.out.println("Username : " + rekening.getAkun().getUsername());
								System.out.println("-----------------------------------");
							} else {
								System.out.println("-----------------------------------");
								System.out.println("Data akun tidak terdaftar, aplikasi");
								System.out.println("akan dialihkan ke Halaman Utama");
								System.out.println("-----------------------------------");
								sleep(500);
								start();
							}
						} else {
							System.out.println("-----------------------------------");
							System.out.println("Rekening tidak terdaftar, aplikasi");
							System.out.println("akan dialihkan ke Halaman Utama");
							System.out.println("-----------------------------------");
							sleep(500);
							start();
						}
					} else if (pilihan == 2) {
						System.out.println("-----------------------------------");
						System.out.println("Data Akun||");
						System.out.println("-----------------------------------");
						System.out.println("Pastikan data akun yang kamu isikan");
						System.out.println("di bawah telah terdaftar di BRImo");
						System.out.print("\nUsername : ");
						username = str.nextLine();
						akun = cariAkun(username);

						if (akun != null) {
							System.out.println("-----------------------------------");
							System.out.println("Masukkan password yang baru");
							System.out.print("Password : ");
							akun.setKataSandi(str.nextLine());
							System.out.println("-----------------------------------");
						} else {
							System.out.println("-----------------------------------");
							System.out.println("Data akun tidak terdaftar, aplikasi");
							System.out.println("akan dialihkan ke Halaman Utama");
							System.out.println("-----------------------------------");
							sleep(500);
							start();
						}
					}
				} else {
					sleep(500);
					start();
				}
			} else {
				isLoggedIn = true;
				menu(akun, isLoggedIn);
			}
		}
	}
	private static void menu(Akun akun, boolean isLoggedIn) {
		while (isLoggedIn) {
			System.out.println("===================================");
			System.out.println("=           Menu Utama            =");
			System.out.println("===================================");
			System.out.println("Selamat datang " + akun.getNamaPengguna() + "!\n");
			System.out.println("1. Cek Saldo Rekening");
			System.out.println("2. Transfer");
			System.out.println("3. BRIVA");
			System.out.println("4. Top Up Pulsa");
			System.out.println("5. Lihat Mutasi Rekening");
			System.out.println("6. Lihat Aktivitas Akun");
			System.out.println("7. Isi Saldo Rekening");
			System.out.println("0. Logout");
			System.out.println("-----------------------------------");
			System.out.print("Pilihan: ");
			int pilihan = in.nextInt();

			sleep(500);
			if (pilihan == 1) {
				while (pilihan != 0) {
					System.out.println("===================================");
					System.out.println("=         List Rekening           =");
					System.out.println("===================================");
					akun.tampilkanListRekening();
					System.out.println("\n1. Buka Rekening Baru");
					System.out.println("0. Kembali");
					System.out.println("-----------------------------------");
					System.out.print("Pilihan: ");
					pilihan = in.nextInt();

					if (pilihan == 1) {
						akun.tambahRekening();
					}
				}
			} else if (pilihan == 2) {
				System.out.println("-----------------------------------");
				System.out.println("|Transfer||");
				System.out.println("-----------------------------------");
				System.out.print("Bank Tujuan : ");
				String bankTujuan = str.nextLine();

				System.out.print("Nomor Tujuan: ");
				String nomorTujuan = str.nextLine();

				System.out.println("-----------------------------------");
				System.out.println("\n    -Tekan ENTER untuk lanjut-");
				str.nextLine();

				System.out.print("Nama Tujuan : ");
				String namaTujuan;
				boolean isRekeningBRI = false;

				if (bankTujuan.equals("Bank BRI") || bankTujuan.equals("BRI") && cariRekening(nomorTujuan) != null) {
					isRekeningBRI = true;
					namaTujuan = cariRekening(nomorTujuan).getNamaRekening();

					System.out.println(namaTujuan);
				} else {
					namaTujuan = str.nextLine();
				}
				System.out.print("Nominal     : ");
				long nominal = in.nextLong();

				System.out.print("Sumber Dana : ");
				Rekening sumberDana;

				if (akun.getRekening().size() != 1) {
					System.out.println();
					akun.tampilkanListRekening();
					System.out.print("\nPilihan: ");
					int indeks = in.nextInt();

					sumberDana = akun.getRekening().get(indeks-1);
				} else {
					sumberDana = akun.getRekening().get(0);

					System.out.println(sumberDana.getNomorRekening());
				}
				int biayaAdmin = 2500;

				System.out.println("Biaya Admin : " + biayaAdmin);
				long totalBiaya = nominal + biayaAdmin;

				System.out.println("Total Biaya : " + totalBiaya);
				System.out.println("\n  -Tekan ENTER untuk konfirmasi-");
				str.nextLine();
				boolean isDone = false;

				if (akun.cekKecukupanSaldo(sumberDana, totalBiaya)) {
					while (!isDone) {
						System.out.println("-----------------------------------");
						System.out.print("Konfirmasi PIN: ");
						String nomorPIN = str.nextLine();

						System.out.println("-----------------------------------");
						if (akun.validasiPIN(nomorPIN)) {
							Transfer transferUang = new Transfer(bankTujuan, nomorTujuan, namaTujuan, nominal, sumberDana, biayaAdmin, totalBiaya);
							transferUang.penguranganSaldo(sumberDana);

							Mutasi mutasi = new Mutasi(("BFST" + nomorTujuan), ("(-) Rp" + nominal), transferUang.getTanggalWaktuTransaksi());
							sumberDana.tambahMutasi(mutasi);

							if (biayaAdmin != 0) {	
								mutasi = new Mutasi(("BFST" + nomorTujuan), ("(-) Rp" + biayaAdmin), transferUang.getTanggalWaktuTransaksi());
								sumberDana.tambahMutasi(mutasi);
							}							
							Aktivitas aktivitas = new Aktivitas("Transfer", (bankTujuan.toUpperCase() + " - " + nomorTujuan), transferUang.getNomorReferensi(), transferUang.getTanggalWaktuTransaksi());
							akun.tambahAktivitas(aktivitas);
							
							if (isRekeningBRI) {
								transferUang.pemindahanSaldo(nomorTujuan);
							}
							sleep(500);
							transferUang.tampilkanBuktiTransaksi();
							System.out.println("\n   -Tekan ENTER untuk kembali-\n");
							str.nextLine();
							isDone = true;
						} else {
							System.out.println("PIN yang dimasukkan salah");
						}
					}
				}
			} else if (pilihan == 3) {
				System.out.println("-----------------------------------");
				System.out.println("|BRIVA||");
				System.out.println("-----------------------------------");
				System.out.print("Nomor Tujuan: ");
				String nomorBRIVA = str.nextLine();
				String institusi = TagihanBRIVA.cekInstitusi(nomorBRIVA);

				System.out.println("Institusi   : " + institusi);
				long nominal = (long) TagihanBRIVA.cekTagihan(institusi);

				System.out.println("Nominal     : " + nominal);
				System.out.println("-----------------------------------");
				System.out.println("\n    -Tekan ENTER untuk lanjut-");
				str.nextLine();

				System.out.print("Sumber Dana : ");
				Rekening sumberDana;

				if (akun.getRekening().size() != 1) {
					System.out.println();
					akun.tampilkanListRekening();
					System.out.print("\nPilihan: ");
					int indeks = in.nextInt();

					sumberDana = akun.getRekening().get(indeks-1);
				} else {
					sumberDana = akun.getRekening().get(0);

					System.out.println(sumberDana.getNomorRekening());
				}
				int biayaAdmin = 0;
				System.out.println("Biaya Admin : " + biayaAdmin);

				long totalBiaya = nominal + biayaAdmin;
				System.out.println("Total Biaya : " + totalBiaya);

				System.out.println("\n  -Tekan ENTER untuk konfirmasi-");
				str.nextLine();
				boolean isDone = false;

				if (akun.cekKecukupanSaldo(sumberDana, totalBiaya)) {
					while (!isDone) {
						System.out.println("-----------------------------------");
						System.out.print("Konfirmasi PIN: ");
						String nomorPIN = str.nextLine();

						System.out.println("-----------------------------------");
						if (akun.validasiPIN(nomorPIN)) {
							TagihanBRIVA bayarBRIVA = new TagihanBRIVA(nomorBRIVA, institusi, nominal, sumberDana, biayaAdmin, totalBiaya);
							bayarBRIVA.penguranganSaldo(sumberDana);

							Mutasi mutasi = new Mutasi((institusi.toUpperCase() + " " + nomorBRIVA), ("(-) Rp" + nominal), bayarBRIVA.getTanggalWaktuTransaksi());
							sumberDana.tambahMutasi(mutasi);

							Aktivitas aktivitas = new Aktivitas("BRIVA", (institusi.toUpperCase() + " - " + nomorBRIVA), bayarBRIVA.getNomorReferensi(), bayarBRIVA.getTanggalWaktuTransaksi());
							akun.tambahAktivitas(aktivitas);

							sleep(500);
							bayarBRIVA.tampilkanBuktiTransaksi();
							System.out.println("\n   -Tekan ENTER untuk kembali-\n");
							str.nextLine();
							isDone = true;
						} else {
							System.out.println("PIN yang dimasukkan salah");
						}
					}
				}
			} else if (pilihan == 4) {
				System.out.println("-----------------------------------");
				System.out.println("|Top Up Pulsa||");
				System.out.println("-----------------------------------");
				System.out.print("Nomor Tujuan: ");
				String nomorTujuan = str.nextLine();
				String provider = TopUpPulsa.cekProvider(nomorTujuan);

				System.out.println("Provider    : " + provider);
				System.out.print("Jumlah Pulsa: ");
				long nominal = in.nextLong();

				System.out.println("-----------------------------------");
				System.out.println("\n    -Tekan ENTER untuk lanjut-");
				str.nextLine();

				System.out.print("Sumber Dana : ");
				Rekening sumberDana;

				if (akun.getRekening().size() != 1) {
					System.out.println();
					akun.tampilkanListRekening();
					System.out.print("\nPilihan: ");
					int indeks = in.nextInt();

					sumberDana = akun.getRekening().get(indeks-1);
				} else {
					sumberDana = akun.getRekening().get(0);

					System.out.println(sumberDana.getNomorRekening());
				}
				int biayaAdmin = TopUpPulsa.cekBiayaAdmin(provider);
				System.out.println("Biaya Admin : " + biayaAdmin);

				long totalBiaya = nominal + biayaAdmin;
				System.out.println("Total Biaya : " + totalBiaya);

				System.out.println("\n  -Tekan ENTER untuk konfirmasi-");
				str.nextLine();
				boolean isDone = false;

				if (akun.cekKecukupanSaldo(sumberDana, totalBiaya)) {
					while (!isDone) {
						System.out.println("-----------------------------------");
						System.out.print("Konfirmasi PIN: ");
						String nomorPIN = str.nextLine();

						System.out.println("-----------------------------------");
						if (akun.validasiPIN(nomorPIN)) {
							TopUpPulsa beliPulsa = new TopUpPulsa(nomorTujuan, provider, nominal, sumberDana, biayaAdmin, totalBiaya);
							beliPulsa.penguranganSaldo(sumberDana);

							Mutasi mutasi = new Mutasi((provider.toUpperCase() + " " + nomorTujuan), ("(-) Rp" + nominal), beliPulsa.getTanggalWaktuTransaksi());
							sumberDana.tambahMutasi(mutasi);

							if (biayaAdmin != 0) {	
								mutasi = new Mutasi((provider.toUpperCase() + " " + nomorTujuan), ("(-) Rp" + biayaAdmin), beliPulsa.getTanggalWaktuTransaksi());
								sumberDana.tambahMutasi(mutasi);
							}	
							Aktivitas aktivitas = new Aktivitas("Pulsa", (provider.toUpperCase() + " - " + nomorTujuan), beliPulsa.getNomorReferensi(), beliPulsa.getTanggalWaktuTransaksi());
							akun.tambahAktivitas(aktivitas);

							sleep(500);
							beliPulsa.tampilkanBuktiTransaksi();
                            System.out.println("\n   -Tekan ENTER untuk kembali-\n");
		    	    	    str.nextLine();
							isDone = true;
						} else {
							System.out.println("PIN yang dimasukkan salah");
						}
					}
				}
			} else if (pilihan == 5) {
				System.out.println("-----------------------------------");
				System.out.println("|Mutasi Rekening||");
				System.out.println("-----------------------------------");
				
				Rekening rekening;
				if (akun.getRekening().size() != 1) {
					System.out.println();
					akun.tampilkanListRekening();
					System.out.print("\nPilihan: ");
					int indeks = in.nextInt();

					rekening = akun.getRekening().get(indeks-1);
				} else {
					rekening = akun.getRekening().get(0);
				}
				System.out.println("Rekening: " + rekening.getNomorRekening());
				System.out.println("-----------------------------------");
				for (int i = rekening.getMutasiRekening().size() - 1; i >= 0; i--) {
					Mutasi currentMutasi = rekening.getMutasiRekening().get(i);

					System.out.println("-----------------------------------");
					System.out.println(currentMutasi.getKeterangan());
					System.out.println(currentMutasi.getNominal());
					System.out.println(currentMutasi.getTanggalWaktu());
					if (i == 0) {
						System.out.println("-----------------------------------");
					}
				}
                System.out.println("\n   -Tekan ENTER untuk kembali-\n");
		    	str.nextLine();
			} else if (pilihan == 6) {
				System.out.println("-----------------------------------");
				System.out.println("|Aktivitas Akun||");
				System.out.println("-----------------------------------");
				for (int i = akun.getAktivitas().size() - 1; i >= 0; i--) {
					Aktivitas currentAktivitas = akun.getAktivitas().get(i);

					System.out.println("-----------------------------------");
					System.out.println(currentAktivitas.getNamaTransaksi());
					System.out.println(currentAktivitas.getKeterangan());
					System.out.println("No. Ref: " + currentAktivitas.getNomorReferensi());
					System.out.println(currentAktivitas.getTanggalWaktu());
					if (i == 0) {
						System.out.println("-----------------------------------");
					}
				}
                System.out.println("\n   -Tekan ENTER untuk kembali-\n");
		    	str.nextLine();
			} else if (pilihan == 7) {
				System.out.println("-----------------------------------");
				System.out.println("|Isi Saldo Rekening||");
				System.out.println("-----------------------------------");
				Rekening rekening;

				if (akun.getRekening().size() != 1) {
					System.out.println();
					akun.tampilkanListRekening();
					System.out.print("\nPilihan: ");
					int indeks = in.nextInt();

					rekening = akun.getRekening().get(indeks-1);
				} else {
					rekening = akun.getRekening().get(0);

					System.out.print("Jumlah: ");
					long nominal = in.nextLong();
					rekening.setSaldoRekening(rekening.getSaldoRekening() + nominal);

					System.out.println("\nSaldo sejumlah " + nominal + " berhasil");
					System.out.println("ditambahkan ke nomor rekening:");
					System.out.println(rekening.getNomorRekening());
					System.out.println("\n   -Tekan ENTER untuk kembali-\n");
				    str.nextLine();
				}
			} else if (pilihan == 0) {
				isLoggedIn = false;

				System.out.println("-----------------------------------");
				System.out.println("Akun telah terlogout");
				System.out.println("-----------------------------------");
				sleep(500);
			}
		}
	}
	private static void menuAdmin() {
		System.out.println("-----------------------------------");
		System.out.println("List Rekening Terdaftar");
		System.out.println("-----------------------------------");
		for (int i = 0; i < rekeningTerdaftar.size(); i++) {
			Rekening currentRekening = rekeningTerdaftar.get(i);

			System.out.println("-----------------------------------");
			System.out.println((i+1) + ". " + currentRekening.getNomorRekening() + " " + currentRekening.getJenisTabungan().getNamaJenisTabungan());
			System.out.println("   " + currentRekening.getNamaRekening());
			System.out.println("   Saldo: " + currentRekening.getSaldoRekening());
			System.out.println("-----------------------------------");
		}
		System.out.println("\n   -Tekan ENTER untuk kembali-\n");
	    str.nextLine();
	}
	private static void loadData() {
		JenisTabungan jenisTabungan = new JenisTabungan("BritAma", 50000, 12500);
		Rekening rekening = new Rekening(jenisTabungan, "37041237851", "Alex Nuhr", 250000);
        rekeningTerdaftar.add(rekening);
        
        Akun akun =  new Akun("Alex Nuhr", "089123456789", "alex.n@gmail.com", "alex123", "qwerty123", "123456");
        akun.tambahRekening(rekening);
        rekening.setAkun(akun);
        akunTerdaftar.add(akun);

		jenisTabungan = new JenisTabungan("Simpedes", 25000, 5500);
		rekening = new Rekening(jenisTabungan, "37041237852", "Budiawan Santoso", 5000000);
        rekeningTerdaftar.add(rekening);
        
        akun =  new Akun("Budiawan Santoso", "081123456789", "budi.s@gmail.com", "budi123", "qwerty123", "123456");
        akun.tambahRekening(rekening);
        rekening.setAkun(akun);
        akunTerdaftar.add(akun);
	}
	private static Akun cariAkun(String username) {
		for (int i = 0; i < akunTerdaftar.size(); i++) {
			if (akunTerdaftar.get(i).getUsername().equals(username)) {
				return akunTerdaftar.get(i);
			}
		}
		return null;
	}
	public static Rekening cariRekening(String nomorRekening) {
		for (int i = 0; i < rekeningTerdaftar.size(); i++) {
			if (rekeningTerdaftar.get(i).getNomorRekening().equals(nomorRekening)) {
				return rekeningTerdaftar.get(i);
			}
		}
		return null;
	}
	public static Rekening bukaRekening() {
		System.out.println("-----------------------------------");
		System.out.println("Buka Rekening||");
		System.out.println("-----------------------------------");
		System.out.print("Nama Lengkap: ");
		String namaPengguna = str.nextLine();

		System.out.println("-----------------------------------");
		System.out.println("Jenis Rekening:");
		System.out.println("1. BritAma");
		System.out.println("2. Simpedes");
		System.out.println("-----------------------------------");
		System.out.print("Pilihan: ");
		int pilihan = in.nextInt();
		String nomorRekening = generateNomorRekening();

		System.out.println("-----------------------------------");
		System.out.println("    -Rekening berhasil dibuat-");
		System.out.println("Nomor Rekening: " + nomorRekening);
		System.out.println("\nSilahkan catat nomor rekeningnya");
		System.out.println("-----------------------------------");
		System.out.println("\n    -Tekan ENTER untuk lanjut-");
		str.nextLine();

		if (pilihan == 1) {
			JenisTabungan jenisTabungan = new JenisTabungan("BritAma", 50000, 12500);
			Rekening rekeningBaru = new Rekening(jenisTabungan, nomorRekening, namaPengguna, 250000);
			rekeningTerdaftar.add(rekeningBaru);

			return rekeningBaru;
		} else if (pilihan == 2) {
			JenisTabungan jenisTabungan = new JenisTabungan("Simpedes", 25000, 5500);
			Rekening rekeningBaru = new Rekening(jenisTabungan, nomorRekening, namaPengguna, 50000);
			rekeningTerdaftar.add(rekeningBaru);

			return rekeningBaru;
		}
		return null;
	}
	private static String generateNomorRekening() {
        StringBuilder nomorRekening = new StringBuilder("37041"); // Menambah prefix ke nomor rekening
        Random random = new Random();
        boolean isUnique = false;

        while (!isUnique) {
        	// Menambah angka acak ke nomor rekening
        	for (int i = 0; i < 6; i++) {
            	nomorRekening.append(random.nextInt(10));
        	}
            // Memeriksa apakah nomor rekening yang dihasilkan sudah ada di dalam data
			if (cariRekening(nomorRekening.toString()) == null) {
				isUnique = true;
			} else {
        		nomorRekening.delete(5, 12); // Menghapus angka acak yang baru dibuat
			}
        }
        return nomorRekening.toString();
    }
	public static void buatAkun() {
		System.out.println("-----------------------------------");
		System.out.println("Daftar BRImo||");
		System.out.println("-----------------------------------");
		System.out.println("Mohon lengkapi data diri Anda!");
		System.out.print("\nNomor Rekening : ");
		String nomorRekening = str.nextLine();
		Rekening rekening = cariRekening(nomorRekening);
		if (rekening != null) {
			if (rekening.getAkun() == null) {
				System.out.print("Nama Lengkap   : ");
				String namaPengguna = str.nextLine();

				System.out.print("Nomor Handphone: ");
				String nomorTelepon = str.nextLine();

				System.out.print("Alamat Email   : ");
				String alamatEmail = str.nextLine();

				System.out.println("\n    -Tekan ENTER untuk lanjut-");
				str.nextLine();
				boolean isCreated = false;

				while (!isCreated) {
					System.out.println("\n-----------------------------------");
					System.out.println("Buat username dan password");
					System.out.print("Username: ");
					String username = str.nextLine();

					System.out.print("Password: ");
					String kataSandi = str.nextLine();

					if (cariAkun(username) != null) {
						System.out.println("-----------------------------------");
						System.out.println("Username tidak tersedia, silahkan");
						System.out.println("buat username yang baru");
					} else if (username.equals(kataSandi)) {
						System.out.println("-----------------------------------");
						System.out.println("Username dan password tidak boleh");
						System.out.println("sama, silahkan buat password baru");
					} else {
						System.out.println("\nBuat nomor PIN");
						System.out.print("PIN: ");
						String nomorPIN = str.nextLine();
						Akun akun = new Akun(namaPengguna, nomorTelepon, alamatEmail, username, kataSandi, nomorPIN);
				
						akun.tambahRekening(rekening);
						akunTerdaftar.add(akun);

						System.out.println("-----------------------------------");
						System.out.println("Akun BRImo berhasil dibuat");
						System.out.println("-----------------------------------");
						isCreated = true;
					}
				}
			} else {
				System.out.println("-----------------------------------");
				System.out.println("Maaf, nomor rekening yang Anda");
				System.out.println("masukkan telah terdaftar di akun");
				System.out.println("dengan username " + rekening.getAkun().getUsername());
				System.out.println("\nAplikasi akan dialihkan ke");
				System.out.println("Halaman Utama");
				System.out.println("-----------------------------------");
				sleep(500);
			}
		} else {
			System.out.println("-----------------------------------");
			System.out.println("Maaf, nomor rekening yang Anda");
			System.out.println("masukkan tidak terdaftar.");
			System.out.println("\nAplikasi akan dialihkan ke");
			System.out.println("Halaman Utama");
			System.out.println("-----------------------------------");
			sleep(500);
		}
	}
	private static void sleep(int miliSecond) {
		try {
			Thread.sleep(miliSecond);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
}