# Tugas Akhir

### Aplikasi:
> BRImo (BRI Mobile)

### Use Case
No | Nama Use Case | Prioritas | Status
--- | --- | --- | --- 
1 | User dapat melakukan transfer uang | Tinggi | **Selesai**
2 | User dapat melakukan transfer internasional | Tinggi | Belum dimulai
3 | User dapat melakukan konversi valas | Tinggi | Belum dimulai
4 | User dapat melihat riwayat pembayaran QR pedagang (bila terdaftar) | Tinggi | Belum dimulai
5 | User dapat menarik tunai | Sedang | Belum dimulai
6 | User dapat menyetor tunai | Sedang | Belum dimulai
7 | User dapat mengisi saldo BRIZZI | Tinggi | Belum dimulai
8 | User dapat membeli paket Pulsa/Data | Tinggi | Dalam pengerjaan
9 | User dapat membeli paket Streaming | Tinggi | Belum dimulai
10 | User dapat mengisi saldo Dompet Digital | Tinggi | Belum dimulai
11 | User dapat membayar tagihan asuransi | Tinggi | Belum dimulai
12 | User dapat membayar tagihan BRIVA | Tinggi | **Selesai**
13 | User dapat membayar tagihan cicilan | Tinggi | Belum dimulai
14 | User dapat membayar tagihan kartu kredit | Tinggi | Belum dimulai
15 | User dapat membayar tagihan listrik | Tinggi | Belum dimulai
16 | User dapat membayar tagihan pasca bayar | Tinggi | Belum dimulai
17 | User dapat membayar tagihan PDAM | Tinggi | Belum dimulai
18 | User dapat membayar tagihan Pendidikan | Tinggi | Belum dimulai
19 | User dapat membayar tagihan SNPMB | Tinggi | Belum dimulai
20 | User dapat membayar tagihan TV Kabel & Internet | Tinggi | Belum dimulai
21 | User dapat membayar tagihan Telkom | Tinggi | Belum dimulai
22 | User dapat melakukan investasi Brights | Tinggi | Belum dimulai
23 | User dapat melakukan investasi DPLK BRI | Tinggi | Belum dimulai
24 | User dapat melakukan investasi e-SBN | Tinggi | Belum dimulai
25 | User dapat melakukan investasi RDN | Tinggi | Belum dimulai
26 | User dapat melakukan investasi Deposito | Tinggi | Belum dimulai
27 | User dapat membayar iuran BPJS | Tinggi | Belum dimulai
28 | User dapat melakukan donasi | Tinggi | Belum dimulai
29 | User dapat membayar biaya perjalanan KAI | Tinggi | Belum dimulai
30 | User dapat membayar biaya perjalanan Travel | Tinggi | Belum dimulai
31 | User dapat membayar pajak, pnbp, dan bea cukai | Tinggi | Belum dimulai
32 | User dapat mendaftar kartu kredit baru | Tinggi | Belum dimulai
33 | User dapat melakukan pinjaman untuk pembayaran langsung | Tinggi | Belum dimulai
34 | User dapat mendaftar iBBIZ untuk pengusaha ritel dan umkm | Tinggi | Belum dimulai
35 | User dapat melakukan pinjaman | Tinggi | Belum dimulai
36 | User dapat melakukan transaksi dari pembayaran terakhir | Sedang | Belum dimulai
37 | User dapat menyimpan target(tujuan) transaksi | Sedang | Belum dimulai
38 | User dapat melakukan pembayaran via QRIS | Tinggi | Belum dimulai
39 | User dapat menghubungkan akun GoPay ke aplikasi | Rendah | Belum dimulai
40 | User dapat melihat mutasi rekening | Tinggi | **Selesai**
41 | User dapat melihat aktivitas transaksi | Tinggi | **Selesai**
42 | User dapat melihat bukti transaksi | Tinggi | Belum dimulai
43 | User dapat mengecek saldo rekening | Tinggi | **Selesai**
44 | User dapat membuka rekening baru | Sedang | **Selesai**
45 | User dapat melihat catatan keuangan | Sedang | Belum dimulai
46 | Public dapat melakukan registrasi | Tinggi | **Selesai**
47 | User dapat login ke aplikasi | Tinggi | **Selesai**
48 | User dapat menerima notifikasi | Rendah | Belum dimulai
49 | User dapat menerima promo spesial | Rendah | Belum dimulai
50 | User dapat mengubah PIN dan Password akun | Sedang | Belum dimulai
51 | User dapat mengubah Fast Menu yang ditampilkan di awal | Rendah | Belum dimulai
52 | User dapat memperbaharui rekening | Rendah | Belum dimulai
53 | User dapat melakukan pengaduan | Rendah | Belum dimulai
54 | User dapat melihat info kurs | Rendah | Belum dimulai
55 | User dapat melihat info saham BRI | Rendah | Belum dimulai
56 | User dapat melihat lokasi atm bri | Rendah | Belum dimulai
57 | User dapat melihat lokasi kantor bri | Rendah | Belum dimulai
58 | User dapat logout dari aplikasi | Tinggi | **Selesai**
59 | Manajemen dapat memberi notifikasi (event, etc.) | Sedang | Belum dimulai 
60 | Manajemen dapat melihat transaksi apa yang sering dilakukan pengguna | Tinggi | Belum dimulai
61 | Manajemen dapat melihat jumlah rekening yang aktif | Tinggi | Belum dimulai
62 | Manajemen dapat melihat jumlah akun yang aktif bertransaksi | Tinggi | Belum dimulai
63 | Manajemen dapat melihat rincian pendapatan bank | Tinggi | Belum dimulai
64 | Manajemen dapat melihat pertumbuhan transaksi | Tinggi | Belum dimulai
65 | Manajemen dapat melihat pertumbuhan pengguna | Tinggi | Belum dimulai

### Class Diagram
```mermaid
classDiagram
    BRImoApp --> Akun
    BRImoApp --> Mutasi
    BRImoApp --> Aktivitas
    BRImoApp --> Transfer
    BRImoApp --> TagihanBRIVA
    BRImoApp --> TopUpPulsa

    Rekening *-- JenisTabungan
    Rekening o-- Mutasi
    Akun *-- Rekening
    Akun o-- Aktivitas
    Mutasi <|-- Aktivitas

    class Transaksi
    <<Abstract>> Transaksi
    Transaksi <|-- Transfer
    Transaksi <|-- TagihanBRIVA
    Transaksi <|-- TopUpPulsa

    class BuktiTransaksi
    <<Interface>> BuktiTransaksi
    BuktiTransaksi -- Transfer
    BuktiTransaksi -- TagihanBRIVA
    BuktiTransaksi -- TopUpPulsa
```
### Video Youtube
- [Belum Bikin]()
